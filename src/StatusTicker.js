/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright © 2022 Mark Mayes */

/*
---------------------------------------------------------
  StatusTicker:
  Show feedback on important events eg steps in WebDAV sync process

---------------------------------------------------------
*/

import { DDD } from "./DDD/CONST.js";
import * as PREF_IDS from "./DDD/PREF_IDS.js";

import { UserData } from "./UserData.js";
import { Storage } from "./Storage.js";

import { __ } from "./utils.js";

class StatusTicker {}

export { StatusTicker };
