/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright © 2022 Mark Mayes */

export const COMBINED_VALUE_STR = 0;
export const CLIENT_ID = 1;
export const JOB_ID = 2;
export const NOTES = 3;
