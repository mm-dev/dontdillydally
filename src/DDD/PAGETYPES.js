/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright © 2022 Mark Mayes */

export const GUIDE = 'guide';
export const WORKSHEET = 'worksheet';
export const CONFIG = 'preferences';
export const JOBSANDCLIENTS = 'jobs-and-clients';
export const REPORTS = 'reports';

