/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright © 2022 Mark Mayes */

export const WORKSHEET_MAIN = 'help-worksheet-main';
export const REPORTS_MAIN = 'help-reports-main';
export const JOBSANDCLIENTS_MAIN = 'help-jobsandclients-main';
export const CONFIG_MAIN = 'help-config-main';
