/* SPDX-License-Identifier: (GPL-3.0-only) */
/* Copyright © 2022 Mark Mayes */

/*
---------------------------------------------------------
  Input:
  Custom `input` element

---------------------------------------------------------
*/

import {__, getElementFromElementOrID} from "./utils.js";
import {DDD} from "./DDD/CONST.js";

class Input extends HTMLElement {
  init(_ob) {
    var prop,
      label_el,
      parent_el = getElementFromElementOrID(_ob.parent);

    this.ob = _ob;

    this.setAttribute("tabindex", 0);
    this.contentEditable = true;
    this.entry_el = _ob.entry_el;

    if (_ob.id) {
      if (_ob.label) {
        label_el = document.createElement("label");
        label_el.innerHTML = _ob.label;
        parent_el.appendChild(label_el);
        label_el.htmlFor = _ob.id;
      }
      this.name = _ob.id;
      this.id = _ob.id;
    }
    if (_ob.class) {
      this.classList.add(_ob.class);
    }
    if (_ob.value) {
      this.value = _ob.value;
    }
    if (_ob.maxlength) {
      this.maxlength = _ob.maxlength;
    }
    if (_ob.placeholder) {
      this.placeholder = _ob.placeholder;
    }
    parent_el.appendChild(this);

    if (_ob.attributes) {
      for (prop in _ob.attributes) {
        this.setAttribute("" + prop, "" + _ob.attributes[prop]);
      }
    }

    this.updateField();
  }

  onInput() {
    this.updateField();
  }

  onFocus(_event) {
    this.updateField();
    this.classList.remove(DDD.CLASSNAMES.PLACEHOLDER);
    _event.stopPropagation();
  }

  onBlur() {
    this.updateField();
  }

  updateField() {
    if (this.value.isEmpty() || this.textContent === this.placeholder) {
      if (document.activeElement === this) {
        this.textContent = "";
      } else {
        this.textContent = this.placeholder;
        this.classList.add(DDD.CLASSNAMES.PLACEHOLDER);
      }
    } else {
      this.value = this.textContent.substr(0, this.maxlength);
      this.classList.remove(DDD.CLASSNAMES.PLACEHOLDER);
    }
  }

  get value() {
    return this.textContent;
  }
  set value(_val) {
    this.textContent = _val;
  }
}

customElements.define("fg-input", Input);
export {Input};
