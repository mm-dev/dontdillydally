//
// To be used with dontdillydally
//
// When lots of work has been done on a repo and ddd hasn't been kept updated,
// this script will automatically convert the log of git commits to JSON, which
// can then be imported into ddd.
//
// Call with eg:
// ```
// node js-git-to-ddd.js since=08/11/2023 clientid=Ctvj9f jobid=J0jtPj
// // or
// node js-git-to-ddd.js since=08/11/2023 clientid=Ctvj9f jobid=J0jtPj combineentries=yes
// ```
//

//
// Node imports
const childProcess = require("child_process");
const readline = require("readline");
const fs = require("fs");

//
// Create short simple GUIDs, this must be the same function that the main app
// calls
const shortID = function (length) {
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
    ret = "";

  for (var i = 0; i < length; i++) {
    ret += chars.charAt(Math.floor(Math.random() * chars.length));
  }

  return ret;
};

// Combine all entries which have the same date into a single parent object
// representing that day. All entries remain separate and have their own GUID.
const rewriteSeparateEntriesForDay = function (_entries) {
  let numEntriesPerDay = Object.keys(_entries).length;
  // Estimate mins for each entry based on typical day length and
  // presumption that each entry took the same amount of time. Round the
  // number to the same increments used in the app.
  let minsPerEntry = MINS_IN_DAY / numEntriesPerDay;
  let remainder = minsPerEntry % ROUND_TO_MINS;
  minsPerEntry -= remainder;

  for (const entryId in _entries) {
    // Rewrite time in the format `T[mins]` eg `T60` = 1 hr
    _entries[entryId][ENTRY_DATA_INDEX_TIME] = "T" + minsPerEntry;
    // Replace dashes with spaces in notes
    _entries[entryId][ENTRY_DATA_INDEX_NOTES] = _entries[entryId][
      ENTRY_DATA_INDEX_NOTES
    ].replaceAll("-", " ");
  }
  return _entries;
};

// Combine all entries which have the same date into a single parent object
// representing that day. All entries are combined into a single day-long task.
// Their notes are combined into a single field for the overall task, separated
// by newline characters.
const rewriteCombinedEntriesForDay = function (_entries) {
  let combinedNotes = "";
  for (const entryId in _entries) {
    combinedNotes +=
      _entries[entryId][ENTRY_DATA_INDEX_NOTES].replaceAll("-", " ") + "\n";
  }
  return {
    [shortID(ENTRY_GUID_LENGTH)]: [
      `T${MINS_IN_DAY}`,
      `${clientId}`,
      `${jobId}`,
      combinedNotes,
    ],
  };
};

//
const ENTRY_GUID_LENGTH = 10;
const ENTRY_ID_PLACEHOLDER = "___ENTRY_GUID___";
const TASK_MINS_PLACEHOLDER = "___TIME_MINS___";
const MINS_IN_DAY = 7 * 60;
const ROUND_TO_MINS = 15;

// The entry data types are derived from their index in the array
const ENTRY_DATA_INDEX_TIME = 0;
const ENTRY_DATA_INDEX_NOTES = 3;

// Process command line arguments

//let sinceDate = "08/11/2023";
//let jobId = "JjCINx";
//let clientId = "CRHINL";
let sinceDate;
let jobId;
let clientId;
let combineEntries = false;
let isMissingArgument = false;

const args = process.argv.slice(2);

for (let i = 0; i < args.length; i++) {
  const namedArgPair = args[i].split("=");

  switch (namedArgPair[0].toLowerCase()) {
    case "jobid":
      jobId = namedArgPair[1];
      break;
    case "clientid":
      clientId = namedArgPair[1];
      break;
    case "since":
      sinceDate = namedArgPair[1];
      break;
    case "combineentries":
      if (
        namedArgPair[1].toLowerCase() === "yes" ||
        namedArgPair[1].toLowerCase() === "true" ||
        namedArgPair[1].toLowerCase() === "1"
      ) {
        combineEntries = true;
      }
      break;
    default:
      break;
  }
}

if (!sinceDate) {
  console.log("Missing argument: 'since=dd/mm/yyyy'");
  isMissingArgument = true;
}
if (!clientId) {
  console.log("Missing argument: 'clientid=C?????'");
  isMissingArgument = true;
}
if (!jobId) {
  console.log("Missing argument: 'jobid=J?????'");
  isMissingArgument = true;
}

if (isMissingArgument) {
  process.exit(1);
}

const outputFilename = combineEntries
  ? `ddd-gitlog-combined-${clientId}-${jobId}.json`
  : `ddd-gitlog-${clientId}-${jobId}.json`;

const days_ob = {};

// Get a formatted git log, with each commit represented as a JSON object,
// returned one line/commit at a time. At this stage, placeholders are used for
// entry GUIDs and times, as git does not have that info. Those values will be
// replaced later on.
const gitLog = childProcess.spawn("git", [
  `log`,
  `--since=${sinceDate}`,
  `--date=format-local:%Y-%m-%d`,
  // For the subject (commit message), use %f, which replaces unsafe characters
  // with dashes, instead of %s which may include quotes and other characters
  // which could mess up parsing.
  `--pretty=format:{ "date": "%cd", "${ENTRY_ID_PLACEHOLDER}": ["${TASK_MINS_PLACEHOLDER}", "${clientId}", "${jobId}", "%f"] }`,
]);

(async function () {
  const rl = readline.createInterface({ input: gitLog.stdout });

  // After all of the lines have been recieved and processed, run through them
  // again, filling in missing details (an estimate of time, and a unique id
  // for each entry). Then write all of the data to a JSON file.
  rl.on("close", function () {
    for (const date_str in days_ob) {
      if (combineEntries) {
        days_ob[date_str] = rewriteCombinedEntriesForDay(days_ob[date_str]);
      } else {
        days_ob[date_str] = rewriteSeparateEntriesForDay(days_ob[date_str]);
      }
    }
    //console.log("days_ob: " + JSON.stringify(days_ob));

    // Wrap in an object under a `days` key, and write to a file
    fs.writeFile(
      outputFilename,
      JSON.stringify({ days: days_ob }),
      function (err) {
        if (err) {
          return console.log(err);
        }
        console.log("\n'" + outputFilename + "' saved to current directory");
        console.log("  - The file can be imported to dontdillydally");
        console.log("  - In the import dialog select 'merge'");
      }
    );
  });

  // As lines are read in, process each (it contains a ddd `entry`)
  for await (const line of rl) {
    let entryId = shortID(ENTRY_GUID_LENGTH);
    let data_str = line.toString().replace(ENTRY_ID_PLACEHOLDER, entryId);

    try {
      let entry_ob = JSON.parse(data_str);
      let dateStr = entry_ob.date;
      if (!(dateStr in days_ob)) {
        days_ob[dateStr] = {};
      }
      days_ob[dateStr][entryId] = entry_ob[entryId];
    } catch (error) {
      console.log("\nERROR: " + error);
      console.log("\tdata_str: " + data_str + "\n");
    }
  }
})();
