- BUG There's a problem with syncing, always regresses to previous state (but does upload current state)
- Revisit dark mode with darkreader etc, look at lines on report tablers, check on kiwi android, ff in ubuntu/termux, ubuntu vm ser4 etc
- Add JS web label https://www.gnu.org/software/librejs/free-your-javascript.html
- Submit to FSF







- Open source it all

- Add weekly/monthly/daily target hours and somehow highlight when targets are reached
- Give more feedback during webdav syncing

- Make and insert all videos listed below
  X Implement as clickable text which opens up a GIF in the popover
  - Add video links to helpItems too

- If days somehow end up spanning more than 1 full year (whatever the year start date may be) - this should be handled gracefully, at the moment you get stuck in the first year on the worksheet

- Add ARIA attributes (eg. hide up/down spinner buttons)

- All themes need to be checked for legibility and any bad visibility issues
  - eg dark theme, jobs and clients, 'add a...' button text is invisible





Implement ink themes
- styles
	- backgrounds all white
	- text all dark
	- borders all mid
	- no shadows
	 - all above inverted in dark mode obvs





# DEMO VIDEOS

- Add time entry
  - Show in window:
    - Existing time entry
    - Time entry to be added
    - Calculation area
  - Hover to show date
  - Select client, job
  - Enter note
  - Select time using both spinners
  - Enter time using keyboard

- Add money entry
  - Show negative
  - Show positive
  - Show calculation field at all times

- Create a report
  - Show date range
  - Show hours/money permutations




# TEST

- Worksheet
  - Add time entry
  - Add money entry (positive)
  - Add money entry (negative)
  - Change job and client on entries
  - Check weekly/monthly/yearly totals are updated correctly
  - View week
    - Use week forward/back buttons
    - Use month forward/back buttons
    - Use 'today' button
  - View month
    - Use week forward/back buttons
    - Use month forward/back buttons
    - Use 'today' button
  - View year
    - Use week forward/back buttons
    - Use month forward/back buttons
    - Use 'today' button
  - Show month totals
  - Show week totals
  - Show no totals

  - All above with keyboard


- Reports
  - Change date range
    - Is range checking correct ie start date has to be before end date
  - Change client
  - Change job
  - Toggle both entry types
  - Results totals, are days / half days calculated according to config


- Jobs & Clients
  - Add a job
  - Rename job
  - Set fg colour
  - Set bg colour
  - Check job on worksheet, are colours and name correct?
  - Add entry using job
  - Delete job, does prompt tell you job is used n times in worksheet?

  - All above for client
  - All above with keyboard


- Config/prefs
  - Try all settings, do they take effect correctly

  - All above with keyboard


- Save Data as file

- Save Data as email


- Clear Data
  - Everything
  - Entries only
  - Check worksheet is updated accordingly

- Import file
  - Clear then repeat for all replace/merge permutations
    - Check worksheet updates accordingly













# DONE

- [ ] BUG create new job, name isn't getting saved 
- [ ] "This jb is used 13 times in the current worksheet, really delete it?" fix 'jb'
- [ ] Clean up/minimise JSON
  - [ ] client/job IDs are in `id` field and duplicated as the object key, use one or the other
  - [ ] `TIME` / `CASH`, find single characters to replace them with eg `T` `M`
  - [ ] Is there a way to make shorter GUIDs? 
    `Math.floor(Math.random() * Date.now()).toString(36).slice(-8);` <-- can start with digits though
    (https://www.madskristensen.net/blog/A-shorter-and-URL-friendly-GUID)
  - [ ] Can date be shorter than `2020-12-30`?

  TODO  make sure piwik is not tracking dontdillydally
  TODO  turn off logging on server
  TODO  add 'year start date' preference
	FIXME	£-0.77 must register as negative (if IS smallUnit && bigUnit=0 && direction===-1 && current===0)
  FIXME final (sheet) totals not calculating correctly
  DONE dots in loaderbar
  DONE  loader should only appear on timesheets page
  DONE  leave gap between months on timesheet
  DONE  horizontal layout of workitem, + X buttons, wrap for portrait
  DONE click around nav while timesheets loading - bug seen where eg. settings page, all settings were duplicated in GUI
  DONE  validate all input data
          time/money
          notes - max length
          client/job names in clients/jobs page
  DONE  after loading file current page must update (redraw) be it timesheets, J&Cs or preferences
  DONE  file load isn't loading data yet
  DONE  workitem remove button shoudnt disappear when firstchild, should remove item then create a new one
        - removeItem should decrement
        - change event should decrement current selection (if it exists) then increment new selection{
	DONE 1st day of following year is showing
  DONE  portrait CSS
        - workItem bottom margin increase
        - notes input move left margin to be right margin on unitSmall
  DONE  remove button should be closer to item it is removing
  DONE  all strings should be constants
  DONE  remove old dynamic classes (jobs/clients) 
	DONE	spinners: numbers should pad eg. 00:45h, £10.00
  DONE  don't need to save 'class' in JSON for each client/job... id holds same info, redundant
  DONE  display month/week start correctly
  DONE  import button wrong colour (white) on rollover
  DONE  notes input field even more faded when its not focused and has no data
  DONE client select dropdown styles broken (CSS not being written after loading data file?)
	DONE	if empty or bad time/money data is **stored in JSON**, correct it to zero
  DONE  number spinners should fade up quickly with short delay (to avoid flickering on 'remove item' etc)
  DONE  clicking anywhere off colorpicker should close it without changing colours (currently working on transparent pixels of png, needs to be the same for all the rest of the screen
  DONE  spinners ony show for hovered/focused day
  DONE if page is changed while timesheets are being drawn, bugs out (clear timeout)
  DONE  ensure big/small units update min/max/step when changing from money to hours or viceversa
  DONE timesheet container not getting scroll focus
  DONE  fix widths of client/job selects on timesheets page, use text-overflow: ellipsis
  DONE  nav buttons shouldnt be chopped off
	DONE	add 'wipe data' buttons with confirmation prompt
  DONE  visual feedback when saving (even though it's happening all the time)...appears on change, fades out after 2 secs
  DONE  jobs/clients
        - boxes fill bg
        - add titles to boxes
  DONE number spinners inconsistent colours
  DONE  use spritesheet png instead of fonts??
  DONE  use updatefrequency to refresh "days drawn" only on % === 0
  DONE icons on buttons too small, positioned badly
  DONE  delete job/client check if any records are referencing them, prompt if so
  DONE  refactor month/week click etc to all use 1 function
  DONE next/prev week/month buttons not working
  DONE  redesign logo
  DONE when first day of timesheet is first of month, don't show previous month totals
  DONE  loading bar
	DONE	select client/job - day remains highlighted (eg. darker date text)
  DONE tab nav - position wrong while page is loading (gap)
	DONE	add year to timesheet special days eg: "January 2018", "2018 week 4", "totals for January 2018"
	DONE	add privacy page/statement
					by default all data is saved in your browser (localStorage)
					you can wipe your data at any time
					you can export your data to a file (in JSON format) and import it into another browser or device
					we don't see any of your personal data
  DONE  remove unnecessary form from clients/job page
  DONE  implement 'used' var in client/job data objects, keep count of how many times it's used on current timesheet
  DONE	blank object being stored in data object results in missing day in UI
  DONE  add week/month/year calculations
  DONE  after updating client/job, styles are not universally updating
  DONE colour palette can push off side of screen resulting in resize on Android
  DONE	 number spinners
  DONE  match all button styles
  DONE 	blank space appears at bottom of page (seems related to LOADING element)
	DONE	clients/jobs page - color pickers do not need to  be checkboxes
  DONE	negative money values should attach negative classname on initial page load
  DONE  borders around inputs and colorpickers on clients/jobs page
	DONE	preference changes not taking effect
  DONE  month/week should flash briefly after day jump
  DONE   jobs and clients list existing jobs/clients
  DONE   jobs and clients proper colour picker
  DONE	 minify JS on save
  DONE  delete temporary <a> created when file is saved
	DONE	'even' class wrongly being applied to child elements
  DONE  month/week skip buttons should auto-repeat
  DONE	 spinners: hour/minute units can wrap
  DONE  'import from file' button doesn't get focus outline
  DONE	 spinners: NaN gets converted to 0
  DONE	spinners: other events should trigger mouseup to prevent stuck spin
	DONE	spinners: unit should be denoted, with £/h and ./:
- [ ] make top bar opaque bg

- [ ] when row is focused clock icon should be white

- [ ] get rid of white fuzzy selection outline/border

- [ ] concept: change timespan of this sheet  
  - [ ] select start date
  - [ ] select end date
- [ ] add new option 'entire sheet' to 'show week/month/year'

- [ ] add info onscreen 'sheet start date:', 'sheet end date:', 'currently showing: [week 23]/[month july]/[year 2019]/[entire sheet]'
- [ ] New page: Reports
  - [ ] Two date pickers to choose date span
  - [ ] Choose a client
    and / or
  - [ ] Choose a job
  - [ ] Potential icons are save in `report-icons`

  TODO  features:
        - keep track of hours worked on a project or for a client
        - track income and expenses
        - see totals by week, month or year
        - colour-code clients and jobs so you can see at a glance where you're spending your time

- [ ] BUG January showingh 28 days in date picker! prob is Feb
- [ ] totals on reports
- [x] empty note should have placeholder colouring.. works until page redraws but then colour is cued from job/client
- [ ] when client name is changed, days data object doesnt change
- [ ] on opening timesheets screen, TODAY'S ROW should be scrolled into view

- [ ] bring all media queries inline
- [ ] clients/jobs page full header should be clickable to add
- [ ] WHen 'today' is a weekend, text inside orange weekday is dull weekend colour
- [ ] jobs/clients: narrow screen, uncollapse side menu, item remove buttons clash overlap 
- first of month insert month name on right next to date
- results: put totals into same table as main results to improve layout
- [ ] add date to report entries
- [ ] use flexbox for side navigation
- [ ] Reports page if no results don't draw table and display "No entries matching your selection, choose something else"
- [ ] Option - sort jobs/clients by most recently used
- [ ] maximise button in wrong state after loading data with minmimised into default/maximised layout
- reports: allow deleting of job or client for fuzzier matching
- smaller shadow around menu buttons etc (for performance)
- sharper/smaller shadow around dropdowns
- BUG full year worksheet date start/end not being substituted (1st jan start date)

- BUG DatePicker
  - classnames should start with a letter, underscore or hyphen (so not eg "2021")
- ddd text in logo, circular or remove
- BUG empty/strips colours are light in dark mode
- Change 'cl' 'jb' when setting up new client/job
- Remove dontdillydally from logo on maximised view
- BUG spinner is doubling-up on touchscreen
-  hover text on month/week/today buttons
- BUG money numeric field doesnt update when filled then clicked outisde, only works with spinners
- BUG ipad Inspect: entry values are clipped (bottom missing)
- BUG on results page, select events are getting called twice --- also is this happening on other pages with selects?
  - Use single click handler on worksheet for all selects (and other elements?)
  - tapping outside a select should always close it
- make arrow keys work in fg-select
- update number field with keyboard, click outside --- field updates, but totals don't
- nav button labels showing in ereader
- mobile: hard to get X to show sometimes, make entry bg clickable
- mobile: layout totals tables

    BUG can type gibberish into number money field and it gets added to placeholder


-  Is it broken on IE11/Edge?
- BUG Reports: negative profit totals show in wrong colour
- BUG flash of colour on eye when it spins in mono scheme
- is there a reason exports are .txt instead of .json?

INSTRUCTIONS
------------

Saving your work
Your browser will save everything automatically and it will be there next time you open the app on that device.
Apple Safari may delete your data without warning. I'm not sure if this will happen but it's possible.
If you click the 'Export' button your data will be downloaded to a file on your computer. As long as you have this file you can re-load your data.

During the testing stage if you are entering real data it is best to Export every day or so. If anything ever goes wrong just send me the exported files and I'll recover your data from them.

Click 'Worksheet' This is the main page where you create entries to track time/money/

Click a date to add an **entry**
- An **entry** can be an amount of time you worked on something
- Or an amount of **money** coming **in**
- Or **money** paid **out** (an expense)

A day can have as many entries as you like

An entry can have a **client** and/or a **job** set
You can enter notes about each entry if you wish

When you first make an entry it will be a **time** entry. There will be a clock icon next to it,

If you click the clock icon it will change to a credit card. This means it is now a money entry.

You can use the spinners (little triangles) to change the amount of time of money. Or you can type iunto the box.

If the amount of money is below zero it means it is money going out. You can type "-10.50" meaning you spent £10.50.

Money going out is displayed in red. Money coming in is green.

---

Click 'Jobs and Clients' (the pencil icon). Here you can create new jobs or clients, rename them and change their colours.

---


Click 'Reports'. Here you can choose a range of dates and a client or job and see how much time you spent on that job and/or client during the selected dates. You can also see how much money came in or was spent on that job/client.




- LOGO has been lazily implemented
  - eye highlight should not rotate
  - mono versions need to be exported
  - PAGEDATA is 'intro' redundant?
-  make home/main screen
  -  accessible by tapping bird
  -  contains large graphical links to all sections with short descriptive paragraph of each

-  make all 'hidden = true' / 'disabled = true' options work or remove them

  TODO  pageIntro from Consts needs to be able to define more complex data (eg. ul, li, icons)
	TODO	look out for autorepeat getting stuck on dayJump (make sure timer gets cancelled on mouseup etc)
  TODO  only show month/week jump buttons if they make sense OR make them work everywhere (ie. flip month/week page)
  TODO  |!| help item
        checkbox which un-shows itself when unchecked
        help icon in main menu - click to make them all show again
        store flags indicating which help disabled/enabled

split header vertically. in right hand side

                                            DAY   WEEK    MONTH   TOTAL
    Ongoing job: General admin (I and I)   02:30  12:00   19:00   322:00         CLOCK OUT
                 (dropdown or /NONE/                                             CLOCK IN


HINTS
------

[ARROW] this is today.




TIMESHEET PAGE
---------------------------------------


MAIN FUNCTIONS

  [ ] add money in
  [ ] add money out

  [ ] clock in (recent job or choose)
  [ ] clock out (recent job or choose)

  [ ] export data
  [ ] import data

  [ ] edit existing entries
      dd/mm/yy    hh:mm   [client]    [job]     [job notes]     [MONEY IN]    [MONEY OUT]   [MONEY NOTES]


SHOW TOTALS
  [ ] hrs/week
  [ ] hrs/month
  [ ] money/week
  [ ] money/month
  [ ] money/net







JOBS AND CLIENTS PAGE
---------------------------------------

  [ ] create new job
  [ ] create new client
      for each of above, choose random (contrasting) colours but allow editing of colours during creation process

  [ ] show hour totals by job
      ()weekly ()monthly ()all-timeg
  [ ] show hour totals by client
      ()weekly ()monthly ()all-time





  [ ] use seasonal background colours (show demo colours and allow shift +/- to match months)

  [ ] show day as:
      day of year
      day of tax year

    J: darker grey on white
    F: black on white
    M: light yellow on muted green
    A: dark yellow on greener green
    M: yellow on light blue
    J: yellowy orange on mid blue
    J: orange on dark blue
    A: orangey-red on dark blue grey
    S: red on deepest blue
    O: brown on bluey grey
    N: orangey on lighter grey
    D: dark grey on light grey

  [ ] first day of month
  [ ] time format ()mm/dd/yy ()dd/mm/yy
  [ ] show totals
      ()weekly ()monthly ()both
  [ ] period to show on timesheets page
      ()week ()month ()year
  [ ] start of accounting period
      (eg. 06/04)
  [ ] smallest time increment worked
      (this is the smallest 'shift' you can do on any one job
    - 1m
    - 5m
    - 15m
    - 30m
    - 1hr
    - 4hr
  FIXME press/hold to jump back through months, page reloads with ? in querystring
  

 CLients/Jobs - don't allow removal of last item
- BUG load a file. colourscheme doesnt take effect
- BUG DatePicker doesnt close sometimes
- BUG reports: sanity-check date ranges (eg end must be after start)

- TODAY changed
  - remove classname from current TODAY
  - remove span 'TODAY' from current TODAY

  IF TODAY is still on visible sheet
  - add classname to new TODAY
  - add span 'TODAY' to new TODAY

- BUG mono light: CC icon still green


- 'special days' rationalise/simplify colours
- BUG MacOS safari select dropdowns appear behind notes filed on narrow layout
- Worksheet don't draw Today button if today not in current year

- Worksheet: should other dates be set to midnight before checking?

  IF TODAY is not in current year
  - disalbe TODAY button
  - change hover to '???'
  - also chec kthis when first drawing timesheet


- every n minutes check to see if today has changed
  if it has, update DOM accordingly (can it be done without a complete redraw?)
-  When starting new year
  tell user what is happening and offer to export old year
  remove 'today' label and disable button
  new blank sheet but keep jobs/clients/settings?? MAYBE DONT AUTOMATE THIS
- BUG 'today' button breaks if today's date is outside spreadsheet (eg on April 6th)

- Reports: date range should be allowed to be 1 day
- top nav draws wrongly while loading, hide until loaded

# Worksheet

- Layout quick settings box
- Remove empty box on page load



# Jobs Clients

- Hover H2 text on add hover


# Reports
- Layout select boxes 







https://www.owasp.org/index.php/HTML5_Security_Cheat_Sheet


https://www.whitehatsec.com/blog/web-storage-security/


The Always and Never of Web Storage
ALWAYS:

Always  validate, encode, and escape user input before placing into localStorage or sessionStorage

Always  validate, encode, and escape data read from localStorage or sessionStorage before writing onto the page (DOM).

Always  treat all data read from localStorage or sessionStorage as untrusted user input.
NEVER:

Never store sensitive data using Web Storage: Web Storage is not secure storage. It is not “more secure” than cookies because it isn’t transmitted over the wire. It is not encrypted. There is no Secure or HTTP only flag so this is not a place to keep session or other security tokens.

Never use Web Storage data for access control decisions or trust the serialized objects you store here for other critical business logic. A malicious user is free to modify their localStorage and sessionStorage values at any time, treat all Web Storage data as untrusted.

Never write stored data to the page (DOM) with a vulnerable JavaScript or library sink.  Here is the best list of JavaScript sinks that I am aware of on the web right now.  While it is true that a perfect storm of tainted data flow must exist for a remote exploit that relies 100% on Web Storage you must consider two alternate scenarios. First, consider the evil roommate, unlocked, unattended, or public computer scenario in which a malicious user has temporary physical access to your user’s web browser. The computer’s owner may have disallowed a low privileged user from installing malicious add-on but I’ve never seen a user prevented from making a bookmark. Second, don’t ignore the possibility of improper Web Storage usage allowing escalation of another vulnerability such as reflective cross-site scripting into persistent cross-site scripting.

- [x] color picker: main.js should listen for resize event and recalculate scales




  TODO  test everything on touchscreen
  TODO  test everything on narrow (phone) layout
  TODO  feedback:
        - [on startup] data and settings restored from previous session (localStorage)
        - [first usage] default data and settings created
        - [on load] data and settings loaded from $filename


- 'epaper'dark/light themes
  - outlines around everything
  - altered contrast to make sure job/client text always very visible
- set body class properly not hacky current way
- create 3 new options in config

- Config
	- layout should use full screen on large viewports (currently only narrow tall column)
- Guide sharpen up copy/abbreviate etc
	- tiny text for tech info

- should be able to tab to '+' entry button


Worksheet drag textarea resize
- layout problems
- handle hidden under number spinners



- weekStartDay is hardcoded - fix


- weekend days can be set in config (Appearance) 


small screens
- make eg "week 13" text "wk13"
- abbreviate weekday to 1 letter
- use text-overflow and :before to achieve above

make work with keyboard
  worksheet remove entry button


- Profits field empty
- totals heading wrong position

- BUG stylesheet not getting created on DDD

- BUG circular dependency


- move as many listeners as possible
  - grep for registerEvntHandlers etc

Reports
- Days/half days need to be clarified and added to config


- is it necessary to use Checkbox class or can normal input els be used now?
- BUG Use last years real data - why is Frantic Fish April payment missing? (has client but no job) when showing all, full year
- Checkboxes: 
	[ ] Show money
	[ ] Show time
spinners when hovered can see grey border


Look into bug 'repeated problem' on iPad is it rotation animation?
	- only happens on worksheet
- Reports
Checkboxes should default to checked
Sheet sizes/CSS doesn't work well
Options should be remembered?? Maybe


- ColorPicker
Hide headings and sheet edges when picker spectrum shows



- color picker use faster method

- length of day should be configurable

- BUG color picker click passes through to hidden elements so can accidentally remove items while changihng colour

Help
- add items to each page
- click to hide
- tick to never show agani / save in prefs

- BUG color picker iOS drag scrolls page need to cancel event
- BUG cant close daterpicker on ipad


Use GUIDs for jobs/clients
BUG - Collapse menu > Clear all data
  Menu remains collapsed, but 'collapse' icon indicates it is not collapsed
  Refresh page and menu redraws and matches icon state
Datepicker year span should add 1 extra year
BUG - on Reports page, select earliest year (2007) as 'from' 
  "maximum call stack size exceeded"
BUG - date selector shows numbers for wrong weekdays when looking at march 2022
BUG - Change entry type from money back to time --- increments are ignored (eg steps of 1 instead of 15)
- Worksheet
BUG enter money '-78.26' using keyboard, corrects to '0.00'

Reports
- Datepicker years should be based on earliest/latest days in `DDD_days`
- Page should show all clients/jobs on initial load
- results overflow horizontally on mobile screen
should reports update initial call be done from GUIDATA (like eg worksheet)
Reports page after clear data - change message if no entries exist at all "No entries exist in the worksheet - go to the worksheet page and add some entries before trying to create reports"
Import: Remove radiobuttongroup before drawing otherwise we get duplicates
BUG: used counts not being updated
popover needs radiobuttongroup
Bug: week number and MON can clash on smaller width screens
totaljobs/totalclients use key instead of number index etc etc
Bug: clear data can be confirmed without selecting radio button
DDD.STRINGS.CLIENTID_PREFIX should be something like DDD.TYPES.CLIENT
if imported jobs/clients are an array, convert to an object first to handle legacy data 
BUG: something wrong with weekend preferences
Allow merge or replace of jobs/clients on import
Styling fix popover buttons - glitch on hover looks like white box around them
ESC  or click outside should close popover
date input fields padding-right missing
datepicker layout on mobile missing buttons, `select` months hang off bottom of screen
after clearing data it's possible to try to make an entry with empty job or client
Windows FF Reports table doesn't fill width of viewport
datepicker layout + popover layout rationalise
BUG: datepipcker confirm without choosing a date results in nan/nan/nan
BUG: config page, year start date is not updating preference
BUG: reporst page, click import. Reports still refreshing/updating on radio click while popover is still showing
Bird logo change worm to ? mark
On all help items add link to guide page
Print CSS - hide help popup items
- Reports: align notes text left
- When importing, allow merging of data
- Checkbox should be squared not round as round is radio button
- BUG: Click video button in help item on worksheet page, helpitem disappears and page sometimes seems to scroll/jump?
- Previous job/client "ditto" button next to new entry button
- BUG: When importing data, it is possible to (Replace) be left with existing day entries which have had their jobs/clients removed
- BUG: Importing wrong/invalid file causes crash, should error
