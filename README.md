# dontdillydally


Track time spent on projects, money coming in, and money going out.

- Aimed at the self-employed
- Focus on simplicity


### Timesheet

A timesheet is made of **entries**. An **entry** is one of the following:
  - **time** spent on a specific job
  - **money** spent on something
  - **money** coming in

Each **entry** can have a **client** and/or a **job** attached.

**Notes** can be added.

_All data fields in an entry are optional._

![Demo: Add Time Entry](www/video/demo-add-time-entry.gif)

![Demo: Add Money Entry](www/video/demo-add-money-entry.gif)


### Clients and Jobs

**Clients** and **jobs** can be defined with customised colours.


### Data Storage

Data is:

- Automatically saved when you input it
- Stored locally inside your browser, it never travels over the internet (unless you decide to send it via email) and is not stored on any servers
- In a simple JSON format, it can be exported/imported as a file to move it between devices


### Roadmap

- Export in other formats eg CSV
- Simple button to clock in/out of current job



# Build Instructions

This is a vanilla JS application. A couple of programs are needed to bundle the JS and process the SASS/CSS. There are lots of options for how to do this, but I've described the general process with examples below.


### JavaScript

The JS is made up of ES Modules, bundled into a single file for production.

Currently I'm using a native binary of [esbuild](https://esbuild.github.io) and the following command to do the bundling:

```
esbuild src/main.js \
--outfile=www/js/dontdillydally.js \
--target=es6 \
--bundle --minify --sourcemap --watch
```

- `src/main.js` is the entry point where code execution will begin
- `www/js/dontdillydally.js` is the final, bundled output

Any bundler which can handle ES Modules can be used. 


### SASS

SASS is used for CSS variables and mixins, and to allow the stylesheet to be split into smaller files for easier management.

Currently using [Ruby SASS](https://sass-lang.com/ruby-sass), and the following command:

`sass --watch src/scss/:www/css/`
